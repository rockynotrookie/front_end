

function strBuildinMethod(){
    let var1= 'abcdef'
    console.log(var1.length) // 6
    console.log(var1[2]) // c 
    console.log(var1.indexOf('bcd'))   // return the start idx , -1 return if got nothing
    console.log(var1.slice(0,-2),var1.slice(1,3)) // abcd  bc
}

function upperorlower(){
    let data  = 'aEioU' 
    console.log(data.toLowerCase())
    console.log(data.toUpperCase())
}

// upperorlower()

function loop(){
    for(let i=0;i<5;i++){
        console.log(i)
    }
}

//loop()

function arr_opt(){
    let arr=[1,2,3]
    arr.push(4)      // 栈 ，后进先出，作用于队尾
    console.log(arr)
    let x = arr.pop()  // 4
    console.log(x)

    arr.unshift(8) // 同上，但是作用于队头
    console.log(arr)
    console.log(arr.shift())
}

arr_opt()

isNaN('ss')
