let myHeading = document.querySelector('h1')
myHeading.textContent = ' hello world';
let myButton = document.querySelector('.callback')

// 分号不是必需的
// 用let 不用var  var 具有变量提升的属性，可以多次声明。


function showData() {
    let a = true
    let b = false

    console.log(a === b) // 等于
    console.log(a !== b)  // 不等于 
    console.log(!a) // 取非 
}


let myImage = document.querySelector('img');
console.log(typeof (myImage))
myImage.onclick = function () {
    let mySrc = myImage.getAttribute("src")
    if (mySrc === 'images/demo1.png') {
        myImage.setAttribute('src', 'images/demo2.png')
    } else {
        myImage.setAttribute('src', 'images/demo1.png')
    }
}


function setUserName() {
    let myName = prompt("请输入你的名字")
    if (!myName || myName == null) {
        setUserName()
        console.log(" invalid name")
    } else {

        localStorage.setItem('name', myName)
        myHeading.textContent = 'chrome is cool ,' + myName
        console.log('get name' + myName)
    }
}

myButton.onclick = function () {
    setUserName();
}

let button1 = document.querySelector('#button1')
let button2 = document.querySelector('#button2')


// change the whole body's background color
button1.onclick = function () {
    const bgColor = 'rgb(' + random(255) + ',' + random(255) + ',' + random(255) + ')'
    document.body.style.backgroundColor = bgColor;
}


// change the element's background color
button2.onclick = function (e) {
    const bgColor = 'rgb(' + random(255) + ',' + random(255) + ',' + random(255) + ')'
    e.target.style.backgroundColor = bgColor;
    console.log(e)
}


function random(num){
    const seed =Math.random()
    return Math.floor(seed*num)
}
